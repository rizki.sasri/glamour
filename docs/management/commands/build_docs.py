import os
import subprocess
import sys

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

User = get_user_model()


class Command(BaseCommand):
    help = "Build documentation site"

    def add_arguments(self, parser):
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Remove old files from the site_dir before building (the default)",
        )

    def handle(self, *args, **options):
        commands = ["mkdocs", "build"]
        source_path = os.path.join(settings.BASE_DIR, "docs")
        if options["clean"]:
            commands.append("--clean")
        try:
            subprocess.run(commands, cwd=source_path)
        except KeyboardInterrupt:
            self.stdout.write("See you soon.")
            sys.exit(0)
