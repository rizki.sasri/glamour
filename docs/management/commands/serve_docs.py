import os
import subprocess
import sys

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

User = get_user_model()


class Command(BaseCommand):
    help = "Serve documentations development server"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        address = "localhost:8001"
        source_path = os.path.join(settings.BASE_DIR, "docs")
        shutdown_message = "See you soon."
        try:
            subprocess.run(["mkdocs", "serve", "-a", address], cwd=source_path)
        except KeyboardInterrupt:
            if shutdown_message:
                self.stdout.write(shutdown_message)
            sys.exit(0)
