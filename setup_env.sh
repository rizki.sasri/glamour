#!/bin/sh
echo USE_TLS=True >> .env
echo ALLOW_WILD_CARD=$ALLOW_WILD_CARD >> .env
echo SITE_DOMAIN=$SITE_DOMAIN >> .env
echo DOMAIN=$DOMAIN >> .env
echo BASE_URL=$BASE_URL >> .env
echo DATABASE_URL=$DATABASE_URL >> .env
echo DEPLOYMENT_ENV=$DEPLOYMENT_ENV >> .env
echo DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE >> .env

echo CLOUDINARY_API_KEY=$CLOUDINARY_API_KEY >> .env
echo CLOUDINARY_API_SECRET=$CLOUDINARY_API_SECRET >> .env
echo CLOUDINARY_CLOUD_NAME=$CLOUDINARY_CLOUD_NAME >> .env

echo DISQUS_ACCOUNT=$DISQUS_ACCOUNT >> .env

echo REDIS_PASSWORD=$REDIS_PASSWORD >> .env
echo REDIS_RENDITION_URL=$REDIS_RENDITION_URL >> .env
echo REDIS_URL=$REDIS_URL >> .env
echo PAGE_CACHE_TIMEOUT=$PAGE_CACHE_TIMEOUT >> .env

echo SECRET_KEY=$SECRET_KEY >> .env

echo SENTRY_DSN=$SENTRY_DSN >> .env
echo STORAGE_TYPE=$STORAGE_TYPE >> .env

echo SMTP_SENDER=$SMTP_SENDER >> .env
echo SMTP_USE_TLS=$SMTP_USE_TLS >> .env
echo SMTP_HOST=$SMTP_HOST >> .env
echo SMTP_PORT=$SMTP_PORT >> .env
echo SMTP_USERNAME=$SMTP_USERNAME >> .env
echo SMTP_PASSWORD=$SMTP_PASSWORD >> .env

echo RECAPTCHA_PRIVATE_KEY=$RECAPTCHA_PRIVATE_KEY >> .env
echo RECAPTCHA_PUBLIC_KEY=$RECAPTCHA_PUBLIC_KEY >> .env

echo GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID >> .env
echo GOOGLE_CLIENT_SECRET=$GOOGLE_CLIENT_SECRET >> .env
echo GOOGLE_CLIENT_KEY=$GOOGLE_CLIENT_KEY >> .env
echo GOOGLE_SITE_VERIFICATION=$GOOGLE_SITE_VERIFICATION >> .env

echo FB_CLIENT_ID=$FB_CLIENT_ID >> .env
echo FB_CLIENT_SECRET=$FB_CLIENT_SECRET >> .env
echo FB_CLIENT_KEY=$FB_CLIENT_KEY >> .env

echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
echo IMAGE_APP=$IMAGE:latest  >> .env
