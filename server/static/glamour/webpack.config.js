const path = require('path');

module.exports = (env, options) => {
    console.log(`Webpack watcher running on: ${options.mode}, time ${new Date().toISOString()}`);
    return {
        entry: "./src",
        output: {
            path: path.resolve(__dirname, 'dist'),
        },
        watch: true,
        module: {
            rules: [
                {
                    test: /\.less$/i,
                    use: [
                        "style-loader",
                        "css-loader",
                        "less-loader"
                    ]
                }
            ]
        }
   }
}
