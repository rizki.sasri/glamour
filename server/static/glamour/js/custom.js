/* Add here all your JS customizations */
tinymce.init({
    selector: 'textarea#id_body',
    menubar: false,
    plugins: 'lists',
    toolbar: 'bold italic underline bullist'
});
