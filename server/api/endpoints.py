# from glamour.pages.api.urls import api_router
from server.api.helpers import get_apiview, get_router

app_name = "v1"

router = get_router("API_V1_VIEWSET_HOOK")
apiviews = get_apiview("API_V1_VIEW_HOOK")
urlpatterns = router.urls + apiviews  # + api_router.get_urlpatterns()
