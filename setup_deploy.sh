#!/bin/sh

ssh -o StrictHostKeyChecking=no root@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
  cd /root/apps/glamour
  rm -rf ./staticfiles || true
  mkdir ./staticfiles || true
  mkdir ./mediafiles || true
  mkdir ./search || true
  mkdir ./logs || true
  chmod 777 ./logs
  chmod 777 ./search
  chmod 777 ./mediafiles
  chmod 777 ./staticfiles
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker image prune -a --force
  docker pull $IMAGE:latest
  docker-compose -f ./docker-compose.yml up -d --force-recreate
ENDSSH
