from django.utils.safestring import mark_safe  # NOQA
from django.utils.translation import gettext_lazy as _  # NOQA
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    RichTextFieldPanel,
    TabbedInterface,
)
from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
    modeladmin_register,
)
from wagtail.images.edit_handlers import ImageChooserPanel

from .models import Community, Partner, Team, Testimonial


class TeamModelAdmin(ModelAdmin):
    model = Team
    list_display = ["name", "job"]
    search_fields = ["name"]
    menu_icon = "user"
    edit_handler = ObjectList(
        [
            MultiFieldPanel(
                [
                    ImageChooserPanel("photo"),
                    FieldPanel("name", classname="title"),
                    FieldPanel("job"),
                    RichTextFieldPanel("description"),
                ],
                heading=_("Information"),
            ),
            MultiFieldPanel(
                [
                    FieldPanel("email"),
                    FieldPanel("phone"),
                    FieldPanel("whatsapp"),
                ],
                heading=_("Contacts"),
            ),
            MultiFieldPanel(
                [
                    FieldPanel("facebook"),
                    FieldPanel("instagram"),
                    FieldPanel("twitter"),
                    FieldPanel("youtube"),
                ],
                heading=_("Contacts"),
            ),
        ]
    )


class PartnerModelAdmin(ModelAdmin):
    model = Partner
    list_display = ["name", "type", "user"]
    list_filter = ["type", "created_at"]
    search_fields = ["name"]
    menu_icon = "user"
    edit_handler = TabbedInterface(
        [
            ObjectList(
                [
                    FieldPanel("name", classname="title"),
                    ImageChooserPanel("logo"),
                    MultiFieldPanel(
                        [
                            FieldPanel("type"),
                            FieldPanel("is_customer"),
                            FieldPanel("is_supplier"),
                            FieldPanel("user"),
                        ],
                        heading=_("Information"),
                    ),
                ],
                heading=_("Details"),
            ),
        ]
    )

    def company_logo(self, obj):
        return obj.logo


class CommunityModelAdmin(ModelAdmin):
    model = Community
    list_display = ["name", "location", "email", "phone", "whatsapp"]
    list_filter = ["location", "created_at"]
    search_fields = ["name", "location"]
    menu_icon = "user"
    edit_handler = ObjectList(
        [
            MultiFieldPanel(
                [
                    ImageChooserPanel("logo"),
                    FieldPanel("name", classname="name"),
                    FieldPanel("location"),
                    RichTextFieldPanel("description"),
                ],
                heading=_("Information"),
            ),
            MultiFieldPanel(
                [
                    FieldPanel("website"),
                    FieldPanel("email"),
                    FieldPanel("phone"),
                    FieldPanel("whatsapp"),
                ],
                heading=_("Contacts"),
            ),
            MultiFieldPanel(
                [
                    FieldPanel("facebook"),
                    FieldPanel("instagram"),
                    FieldPanel("twitter"),
                    FieldPanel("youtube"),
                ],
                heading=_("Contacts"),
            ),
            InlinePanel("gallery_images", heading=_("Gallery"),),
        ]
    )


class TestimonialModelAdmin(ModelAdmin):
    model = Testimonial
    list_display = ["name", "job", "company"]
    menu_icon = "comment"
    edit_handler = TabbedInterface(
        [
            ObjectList(
                [
                    MultiFieldPanel(
                        [
                            ImageChooserPanel("photo"),
                            FieldPanel("name"),
                            FieldPanel("job"),
                            FieldPanel("company"),
                        ],
                        heading=_("Information"),
                    ),
                    RichTextFieldPanel("content"),
                ],
                heading=_("Details"),
            ),
        ]
    )


class PartnerAdminGroup(ModelAdminGroup):
    items = (TeamModelAdmin, PartnerModelAdmin, CommunityModelAdmin, TestimonialModelAdmin)
    menu_label = _("Partners")
    menu_icon = "group"


modeladmin_register(PartnerAdminGroup)
