from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from phonenumber_field.modelfields import PhoneNumberField
from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.models import Orderable, ParentalKey


class Partner(models.Model):

    PERSONAL = "personal"
    ORGANIZATION = "organization"
    TYPES = (
        (PERSONAL, _("Personal")),
        (ORGANIZATION, _("Organization")),
    )

    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("logo"),
        help_text=_("Company logo or photo profile."),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
        help_text=_("Partner name can be personal or company name."),
    )
    type = models.CharField(
        max_length=25,
        choices=TYPES,
        default=ORGANIZATION,
        verbose_name=_("Type"),
    )
    is_customer = models.BooleanField(default=True)
    is_supplier = models.BooleanField(default=False)

    description = RichTextField(
        null=True,
        blank=True,
        verbose_name=_("Describe your partner."),
    )

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    created_at = models.DateTimeField(
        default=timezone.now,
        editable=False,
        verbose_name=_("created at"),
    )

    def __str__(self):
        return self.name


class Team(models.Model):
    photo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("photo"),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
    )
    job = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        verbose_name=_("job"),
    )
    description = RichTextField(
        verbose_name=_("What people said?"),
    )

    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
    )
    phone = PhoneNumberField(
        null=True, blank=True, verbose_name=_("phone"), help_text=_("Enter a valid phone number (e.g. +12125552368).")
    )
    whatsapp = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("whatsapp"),
        help_text=_("Enter a valid whatsapp number (e.g. +12125552368)."),
    )
    facebook = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("facebook"),
    )
    instagram = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("instagram"),
    )
    youtube = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("youtube"),
    )
    twitter = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("twitter"),
    )

    class Meta:
        verbose_name = _("Team")
        verbose_name_plural = _("Teams")

    def __str__(self):
        return self.name


class Community(ClusterableModel, models.Model):
    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("logo"),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
    )
    description = RichTextField(
        verbose_name=_("description"),
    )
    location = models.CharField(
        max_length=255,
        verbose_name=_("location"),
    )

    website = models.URLField(
        null=True,
        blank=True,
        verbose_name=_("website"),
    )
    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
    )
    phone = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("phone"),
        help_text=_("Enter a valid phone number (e.g. +12125552368)."),
    )
    whatsapp = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("whatsapp"),
        help_text=_("Enter a valid whatsapp number (e.g. +12125552368)."),
    )
    facebook = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("facebook"),
    )
    instagram = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("instagram"),
    )
    youtube = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("youtube"),
    )
    twitter = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("twitter"),
    )

    created_at = models.DateTimeField(
        default=timezone.now,
        editable=False,
        verbose_name=_("created at"),
    )

    class Meta:
        verbose_name = _("Community")
        verbose_name_plural = _("Communities")

    def __str__(self):
        return self.name


class CommunityGallery(Orderable):
    page = ParentalKey(
        Community,
        on_delete=models.CASCADE,
        related_name="gallery_images",
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.CASCADE,
        related_name="+",
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        FieldPanel("image"),
        FieldPanel("caption"),
    ]


class Testimonial(models.Model):
    photo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("photo"),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
    )
    job = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        verbose_name=_("job"),
    )
    content = RichTextField(
        verbose_name=_("content"),
        help_text=_("What people said?"),
    )

    company = models.ForeignKey(
        Partner,
        related_name="testimonials",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    created_at = models.DateTimeField(
        default=timezone.now,
        editable=False,
        verbose_name=_("created at"),
    )

    def __str__(self):
        return f"{self.name} - {self.company}"
