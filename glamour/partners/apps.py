from django.apps import AppConfig


class GlamourAppsPartnersConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glamour.partners"
    label = "glamour_partners"
