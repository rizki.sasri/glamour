from django.apps import AppConfig


class GlamourAppsNavsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glamour.navs"
    label = "glamour_navs"
