from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DetailView
from django_filters.views import FilterView

from .models import Car, CarConsignment, CarInquiry


class CarListView(FilterView):
    model = Car
    filterset_fields = ("brand",)
    template_name = "glamour_cars/car_list.html"
    extra_context = {"title": _("All cars")}


class CarDetailView(DetailView):
    model = Car
    template_name = "glamour_cars/car_detail.html"


@method_decorator([login_required], name="dispatch")
class CreateInquiryView(CreateView):
    model = CarInquiry
    template_name = "glamour_cars/form_inquiry.html"
    fields = ("car", "full_name", "email", "phone", "message")
    success_url = reverse_lazy("account_profile")
    extra_context = {"title": _("Request my car")}

    def dispatch(self, request, slug, *args, **kwargs):
        self.car = get_object_or_404(Car, slug=slug)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_initial(self):
        return {"car": self.car, "full_name": self.request.user.get_full_name()}


@method_decorator([login_required], name="dispatch")
class CreateConsignmentView(CreateView):
    model = CarConsignment
    template_name = "glamour_cars/form_consignment.html"
    fields = "__all__"
    extra_context = {"title": _("Sell my car")}
