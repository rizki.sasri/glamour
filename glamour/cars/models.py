from django.conf import settings
from django.db import models  # NOQA
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from phonenumber_field.modelfields import PhoneNumberField
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, RichTextFieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable

from glamour.products.models import Product
from glamour.utils.slug import unique_slugify


class Brand(models.Model):
    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("logo"),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
    )
    slug = models.SlugField(
        unique=True,
        null=True,
        blank=True,
        editable=False,
        max_length=255,
        db_index=True,
    )
    description = RichTextField(
        null=True,
        blank=True,
        verbose_name=_("description"),
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, self.name, queryset=Brand.objects.all())
        super().save(*args, **kwargs)


class TaggedCar(TaggedItemBase):
    content_object = ParentalKey(
        "Car",
        related_name="tagged_items",
        on_delete=models.CASCADE,
    )

    class Meta(TaggedItemBase.Meta):
        verbose_name = _("Tags")
        verbose_name_plural = _("Content Tags")


class Car(ClusterableModel, Product):

    NEW_CAR = "new-car"
    USED_CAR = "used-car"
    CONDITION = (
        (NEW_CAR, _("New Car")),
        (USED_CAR, _("Used Car")),
    )
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("thumbnail"),
    )
    tags = ClusterTaggableManager(through=TaggedCar, blank=True)
    brand = models.ForeignKey(
        Brand,
        related_name="cars",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        verbose_name=_("Brand"),
        help_text=_("Select car brand."),
    )
    description = RichTextField(
        verbose_name=_("description"),
        null=True,
        blank=False,
        help_text=_("Describe the here."),
    )

    price = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        default=0.00,
        verbose_name=_("Price"),
    )

    condition = models.CharField(
        choices=CONDITION,
        default=NEW_CAR,
        verbose_name=_("Condition"),
        max_length=255,
        help_text=_("Filter by car conditions"),
    )
    previous_owner = models.CharField(
        null=True,
        blank=True,
        verbose_name=_("Previous Owner"),
        max_length=255,
        help_text=_("Used for used car."),
    )

    # technical specifications
    interior_colour = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name=_("interior colour"),
    )
    exterior_colour = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name=_("exterior colour"),
    )
    year = models.CharField(
        max_length=6,
        blank=True,
        null=True,
        verbose_name=_("year"),
    )
    engine = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name=_("engine"),
    )
    tax_year = models.DateField(
        default=timezone.now,
        blank=True,
        null=True,
        verbose_name=_("tax_year"),
    )
    last_service = models.DateField(
        default=timezone.now,
        blank=True,
        null=True,
        verbose_name=_("last_service"),
    )
    mileage = models.IntegerField(
        default=0,
        blank=True,
        null=True,
        verbose_name=_("mileage"),
    )

    class Meta:
        verbose_name = _("Car")
        verbose_name_plural = _("Cars")

    def save(self, *args, **kwargs):
        self.title = self.name
        if not self.slug:
            unique_slugify(self, self.name, queryset=Product.objects.all())
        super().save(*args, **kwargs)


class CarPricing(Orderable):
    CUSTOM_DEFAULT = "custom-default"
    PRICING_NAMES = ((CUSTOM_DEFAULT, _("Custom Default")),)
    car = ParentalKey(
        Car,
        related_name="pricings",
        on_delete=models.CASCADE,
    )
    type = models.CharField(
        choices=PRICING_NAMES,
        default=CUSTOM_DEFAULT,
        verbose_name=_("Condition"),
        max_length=255,
        help_text=_("Filter by car conditions"),
    )
    price = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        default=0.00,
        verbose_name=_("Price"),
    )

    class Meta:
        verbose_name = _("Car Pricing")
        verbose_name_plural = _("Car Pricings")
        unique_together = ("car", "type")

    def __str__(self):
        return f"{self.car} - {self.type}"


class CarSpecification(Orderable):
    car = ParentalKey(
        Car,
        related_name="specifications",
        on_delete=models.CASCADE,
    )
    specification = models.TextField(
        max_length=255,
        verbose_name=_("specification"),
    )

    class Meta:
        verbose_name = _("Car Specification")
        verbose_name_plural = _("Car Specifications")


class CarGalleryImage(Orderable):
    page = ParentalKey(
        Car,
        on_delete=models.CASCADE,
        related_name="gallery_images",
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.CASCADE,
        related_name="+",
        verbose_name=_("image"),
    )
    caption = models.CharField(
        blank=True,
        max_length=250,
        verbose_name=_("caption"),
    )

    panels = [
        FieldPanel("image"),
        FieldPanel("caption"),
    ]

    class Meta:
        verbose_name = _("Car Gallery")
        verbose_name_plural = _("Car Galleries")


class CarInquiry(models.Model):
    WAITING = "waiting"
    MODERATED = "moderated"
    STATUS = (
        (WAITING, _("Waiting for Moderation")),
        (MODERATED, _("Moderated")),
    )
    status = models.CharField(
        choices=STATUS,
        default=WAITING,
        max_length=50,
        verbose_name=_("Status"),
    )
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("owner"),
        null=True,
        blank=True,
        editable=True,
        on_delete=models.SET_NULL,
        related_name="inquiries",
    )
    car = models.ForeignKey(
        Car,
        related_name="inquiries",
        on_delete=models.PROTECT,
        null=True,
        blank=False,
    )
    full_name = models.CharField(
        max_length=50,
        verbose_name=_("full name"),
    )
    phone = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("phone"),
        help_text=_(
            "Enter a valid phone number (e.g. +12125552368).",
        ),
    )
    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
    )
    message = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("message"),
    )

    class Meta(TaggedItemBase.Meta):
        verbose_name = _("Inquiry")
        verbose_name_plural = _("Inquiries")

    def __str__(self):
        title = f"{self.full_name}"
        if self.car:
            title += f" - {self.car}"
        return title


class CarConsignment(ClusterableModel, models.Model):
    WAITING = "waiting"
    MODERATED = "moderated"
    STATUS = (
        (WAITING, _("Waiting for Moderation")),
        (MODERATED, _("Moderated")),
    )
    status = models.CharField(
        choices=STATUS,
        default=WAITING,
        max_length=50,
        verbose_name=_("Status"),
    )
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("owner"),
        null=True,
        blank=True,
        editable=True,
        on_delete=models.SET_NULL,
        related_name="car_submissions",
    )
    full_name = models.CharField(
        max_length=50,
        verbose_name=_("full name"),
    )
    phone = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("phone"),
        help_text=_(
            "Enter a valid phone number (e.g. +12125552368).",
        ),
    )
    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
    )

    # Car Info
    brand = models.ForeignKey(
        Brand,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="submissions",
    )
    brand_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("other brand"),
    )
    message = RichTextField(
        null=True,
        blank=True,
        verbose_name=_("message"),
    )

    panels = [
        FieldPanel("full_name"),
        FieldPanel("phone"),
        RichTextFieldPanel("message"),
        InlinePanel("gallery_images", label="Gallery images"),
    ]

    class Meta(TaggedItemBase.Meta):
        verbose_name = _("Consignment")
        verbose_name_plural = _("Consignments")

    def __str__(self):
        title = f"{self.full_name}"
        if self.brand_name:
            title += f" - {self.brand_name}"
        return title


class CarConsignmentGallery(Orderable):
    submission = ParentalKey(
        CarConsignment,
        on_delete=models.CASCADE,
        related_name="gallery_images",
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.CASCADE,
        related_name="+",
        verbose_name=_("image"),
    )

    panels = [
        FieldPanel("image"),
    ]

    class Meta:
        verbose_name = _("Car Consignment Gallery")
        verbose_name_plural = _("Car Consignment Galleries")
