from django.urls import path

from . import views

urlpatterns = [
    path("", views.CarListView.as_view(), name="cars_car_list"),
    path("consignment/", views.CreateConsignmentView.as_view(), name="cars_create_consignment"),
    path("<str:slug>/", views.CarDetailView.as_view(), name="cars_car_detail"),
    path("<str:slug>/inquiry/", views.CreateInquiryView.as_view(), name="cars_create_inquiry"),
]
