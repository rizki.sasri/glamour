from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _  # NOQA
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    RichTextFieldPanel,
    TabbedInterface,
)
from wagtail.contrib.modeladmin.helpers import PermissionHelper
from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
    modeladmin_register,
)
from wagtail.images.edit_handlers import ImageChooserPanel

from .models import Brand, Car, CarConsignment, CarInquiry


class ReadOnlyPermissionHelper(PermissionHelper):
    def user_can_create(self, user):
        return False

    def user_can_edit_obj(self, user, obj):
        return False

    # def user_can_delete_obj(self, user, obj):
    #     return False

    # def user_can_inspect_obj(self, user, obj):
    #     return True


class BrandModelAdmin(ModelAdmin):
    model = Brand
    list_display = ["name", "car_description"]
    menu_icon = "form"

    def car_description(self, obj):
        return mark_safe(obj.description)


class CarModelAdmin(ModelAdmin):
    model = Car
    menu_icon = "form"
    list_display = ["name", "brand"]
    list_filter = ["live", "locked", "condition", "created_at"]
    search_fields = ["name", "brand__name"]

    # Edit Handlers
    content_panels = [
        FieldPanel("name"),
        ImageChooserPanel("thumbnail"),
        MultiFieldPanel(
            [
                FieldPanel("brand"),
                FieldPanel("tags"),
                RichTextFieldPanel("description"),
            ],
            heading=_("SEO Settings"),
        ),
        InlinePanel("gallery_images", label=_("Gallery images")),
    ]

    specification_panels = [
        FieldPanel("condition"),
        MultiFieldPanel(
            [
                FieldPanel("year"),
                FieldPanel("tax_year"),
                FieldPanel("engine"),
                FieldPanel("last_service"),
                FieldPanel("mileage"),
                FieldPanel("exterior_colour"),
                FieldPanel("exterior_colour"),
                FieldPanel("previous_owner"),
            ],
            heading=_("Technical Specifications"),
        ),
        InlinePanel("specifications", label=_("Extra Specifications")),
    ]

    settings_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
            ],
            heading=_("SEO Settings"),
        ),
        MultiFieldPanel(
            [
                FieldPanel("go_live_at"),
                FieldPanel("expire_at"),
            ],
            heading=_("Scheduled publishing"),
        ),
    ]
    edit_handler = TabbedInterface(
        [
            ObjectList(content_panels, heading=_("Content")),
            ObjectList(specification_panels, heading=_("Specifications")),
            ObjectList(settings_panels, heading=_("Settings"), classname="settings"),
        ]
    )


class InquiryModelAdmin(ModelAdmin):
    permission_helper_class = ReadOnlyPermissionHelper
    model = CarInquiry
    list_display = ["owner", "car", "full_name", "phone", "email", "status"]
    list_filter = ["status", "created_at"]
    menu_icon = "openquote"
    search_fields = ["car__title", "email", "phone"]


class CarConsignmentModelAdmin(ModelAdmin):
    permission_helper_class = ReadOnlyPermissionHelper
    inspect_view_enabled = True
    model = CarConsignment
    list_display = ["full_name", "brand", "email", "phone"]
    list_filter = ["status", "created_at"]
    search_fields = ["full_name", "email", "phone"]
    menu_icon = "openquote"


class ShowroomAdminGroup(ModelAdminGroup):
    items = (
        CarModelAdmin,
        BrandModelAdmin,
        InquiryModelAdmin,
        CarConsignmentModelAdmin,
    )
    menu_label = _("Showroom")
    menu_order = None
    menu_icon = "form"


modeladmin_register(ShowroomAdminGroup)
