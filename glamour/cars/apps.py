from django.apps import AppConfig


class GlamourAppsCarsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glamour.cars"
    label = "glamour_cars"
