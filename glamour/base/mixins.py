from django.apps import apps
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


def get_default_page_content_type():
    # Geet
    return apps.get_model()


class PageMixin(models.Model):

    show_in_menus_default = False

    title = models.CharField(
        verbose_name=_("title"),
        max_length=255,
        help_text=_("The page title as you'd like it to be seen by the public"),
    )
    slug = models.SlugField(
        unique=True,
        null=True,
        blank=True,
        editable=False,
        max_length=255,
        db_index=True,
        allow_unicode=True,
        help_text=_("The name of the page as it will appear in URLs e.g http://domain.com/blog/[my-slug]/"),
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("owner"),
        null=True,
        blank=True,
        editable=True,
        on_delete=models.SET_NULL,
        related_name="owned_%(class)s",
    )

    seo_title = models.CharField(
        verbose_name=_("title tag"),
        max_length=255,
        blank=True,
        help_text=_("The name of the page displayed on search engine results as the clickable headline."),
    )
    search_description = models.TextField(
        verbose_name=_("meta description"),
        blank=True,
        help_text=_("The descriptive text displayed underneath a headline in search engine results."),
    )

    live = models.BooleanField(verbose_name=_("live"), default=True, editable=False)
    expired = models.BooleanField(verbose_name=_("expired"), default=False, editable=False)
    show_in_menus = models.BooleanField(
        verbose_name=_("show in menus"),
        default=False,
        help_text=_("Whether a link to this page will appear in automatically generated menus"),
    )
    go_live_at = models.DateTimeField(verbose_name=_("go live date/time"), blank=True, null=True)
    expire_at = models.DateTimeField(verbose_name=_("expiry date/time"), blank=True, null=True)

    locked = models.BooleanField(verbose_name=_("locked"), default=False, editable=False)
    locked_at = models.DateTimeField(verbose_name=_("locked at"), null=True, editable=False)
    locked_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("locked by"),
        null=True,
        blank=True,
        editable=False,
        on_delete=models.SET_NULL,
        related_name="locked_%(class)s",
    )

    first_published_at = models.DateTimeField(
        verbose_name=_("first published at"), blank=True, null=True, db_index=True
    )
    last_published_at = models.DateTimeField(verbose_name=_("last published at"), null=True, editable=False)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.title is None:
            self.title = str(self)
        super().save(*args, **kwargs)
