from django.apps import AppConfig


class GlamourAppsBaseConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glamour.base"
    label = "glamour_base"
