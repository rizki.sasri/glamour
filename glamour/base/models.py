from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
    ObjectList,
    RichTextFieldPanel,
    TabbedInterface,
)
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel


class CompanySetting(BaseSetting):

    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("logo"),
        help_text=_("Company logo or photo profile."),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
        help_text=_("Partner name can be personal or company name."),
    )
    description = RichTextField(
        null=True,
        blank=True,
        verbose_name=_("description"),
        help_text=_("Describe your partner."),
    )
    address = models.TextField(null=True, blank=True, verbose_name=_("address line"))
    google_map = models.TextField(null=True, blank=True, verbose_name=_("Google Map url"))

    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
    )
    phone = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("phone"),
        help_text=_("Enter a valid phone number (e.g. +12125552368).")
    )
    whatsapp = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("whatsapp"),
        help_text=_("Enter a valid whatsapp number (e.g. +12125552368).")
    )
    facebook = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("facebook"),
    )
    instagram = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("instagram"),
    )
    youtube = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("youtube"),
    )
    twitter = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("twitter"),
    )

    edit_handler = TabbedInterface(
        [
            ObjectList(
                [
                    ImageChooserPanel("logo"),
                    FieldPanel("name", classname="title"),
                    RichTextFieldPanel("description"),
                    MultiFieldPanel(
                        [
                            FieldPanel("address"),
                            FieldPanel("google_map"),
                        ],
                        heading=_("Address"),
                    ),
                    MultiFieldPanel(
                        [
                            FieldPanel("email"),
                            FieldPanel("phone"),
                            FieldPanel("whatsapp"),
                        ],
                        heading=_("Contacts"),
                    ),
                    MultiFieldPanel(
                        [
                            FieldPanel("facebook"),
                            FieldPanel("instagram"),
                            FieldPanel("youtube"),
                            FieldPanel("twitter"),
                        ],
                        heading=_("Social Media"),
                    ),
                ],
                heading=_("Details"),
            ),
        ]
    )


register_setting(CompanySetting)
