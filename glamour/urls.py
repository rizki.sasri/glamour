"""
    Contains all glamour apps url patterns. Every apps urls should be registered in this file.
"""
from django.urls import include, path

from .auth import urls as auth_urls
from .cars import urls as cars_urls

urlpatterns = [
    path("cars/", include(cars_urls)),
    path("accounts/", include(auth_urls)),
]
