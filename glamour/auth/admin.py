from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import Deactivation


@admin.register(Deactivation)
class DeactivationAdmin(admin.ModelAdmin):
    list_display = ["user", "started_at", "deactivated_at", "time_left", "executed_at"]


@admin.register(get_user_model())
class UserAdmin(BaseUserAdmin):
    list_display = [
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_superuser",
        "is_active",
        "deactivation",
    ]
