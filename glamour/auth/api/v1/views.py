from djoser.views import UserViewSet as BaseUserViewSet

from .serializers import UserWithPermissionSerializer


class UserViewSet(BaseUserViewSet):
    pass

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return UserWithPermissionSerializer
        elif self.action == "me" and self.request.method == "GET":
            return UserWithPermissionSerializer
        else:
            return super().get_serializer_class()
