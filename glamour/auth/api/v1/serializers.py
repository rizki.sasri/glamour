from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.db import models
from djoser.conf import settings
from rest_framework import serializers

from glamour.auth.models import Deactivation

User = get_user_model()


class PermissionListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        """List of permission codename."""
        iterable = data.all() if isinstance(data, models.Manager) else data

        return [item.codename for item in iterable]


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ["name", "codename"]
        # list_serializer_class = PermissionListSerializer


class GroupListSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        """List of group name."""
        iterable = data.all() if isinstance(data, models.Manager) else data
        return [item.name for item in iterable]


class GroupSerializer(serializers.ModelSerializer):
    permissions = PermissionSerializer(many=True)

    class Meta:
        model = Group
        fields = ["name", "permissions"]
        # list_serializer_class = GroupListSerializer


class DeactivationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deactivation
        fields = ["user", "started_at", "deactivated_at", "time_left"]


class UserWithPermissionSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    user_permissions = PermissionSerializer(many=True, read_only=True)
    deactivation = DeactivationSerializer(read_only=True)

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            settings.USER_ID_FIELD,
            settings.LOGIN_FIELD,
            "is_staff",
            "is_superuser",
            "is_active",
            "groups",
            "user_permissions",
            "deactivation",
        )
        read_only_fields = (settings.LOGIN_FIELD,)
