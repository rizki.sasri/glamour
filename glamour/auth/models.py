from datetime import timedelta
from functools import cached_property

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timesince, timezone
from django.utils.translation import gettext_lazy as _  # NOQA
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):

    phone = PhoneNumberField(
        null=True, blank=True, verbose_name=_("phone"), help_text=_("Enter a valid phone number (e.g. +12125552368).")
    )
    whatsapp = PhoneNumberField(
        null=True,
        blank=True,
        verbose_name=_("whatsapp"),
        help_text=_("Enter a valid whatsapp number (e.g. +12125552368)."),
    )
    facebook = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("facebook"),
    )
    instagram = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("instagram"),
    )
    youtube = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("youtube"),
    )
    twitter = models.CharField(
        null=True,
        blank=True,
        max_length=125,
        verbose_name=_("twitter"),
    )

    def deactivate(self):
        if self.deactivation is None:
            print("can deactivated")

    def get_full_name(self) -> str:
        full_name = "%s %s" % (self.first_name, self.last_name)
        return self.username if full_name.strip() == "" else full_name.strip()


class Deactivation(models.Model):

    user = models.OneToOneField(User, verbose_name=_("user"), on_delete=models.CASCADE)
    started_at = models.DateTimeField(default=timezone.now)
    executed_at = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Deactivation")
        verbose_name_plural = _("Deactivations")

    def __str__(self):
        return self.time_left

    @cached_property
    def deactivated_at(self):
        return self.started_at + timedelta(days=14)

    @cached_property
    def time_left(self):
        return timesince.timeuntil(self.deactivated_at)


class Preference(models.Model):

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="preferences",
        verbose_name=_("User"),
    )

    # Notification settings
    car_notification = models.BooleanField(
        default=True,
        verbose_name=_("Car Notice"),
        help_text=_("Receive car notification"),
    )
    event_notification = models.BooleanField(
        default=True,
        verbose_name=_("Event Notice"),
        help_text=_("Receive event notification"),
    )
    news_notification = models.BooleanField(
        default=True,
        verbose_name=_("News Notice"),
        help_text=_("Receive news notification"),
    )
    community_notification = models.BooleanField(
        default=True,
        verbose_name=_("Community Notice"),
        help_text=_("Receive community notification"),
    )

    class Meta:
        verbose_name = _("Preference")
        verbose_name_plural = _("Preferences")

    def __str__(self):
        return f"{self.user}"
