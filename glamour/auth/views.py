from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView


@method_decorator([login_required], name="dispatch")
class AccountProfileView(TemplateView):
    template_name = "account/profile.html"
    extra_context = {"title": _("My Profile")}
