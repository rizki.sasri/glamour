from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GlamourAppsAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'glamour.auth'
    label = 'glamour_auth'
    verbose_name = _("Authentications")
