from allauth import urls as allauth_url
from django.urls import include, path
from notifications import urls as notification_urls

from . import views

urlpatterns = [
    path("notifications/", include(notification_urls)),
    path("profile/", views.AccountProfileView.as_view(), name="account_profile"),
    path("", include(allauth_url)),
]
