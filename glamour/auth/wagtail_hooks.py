from djoser.views import TokenCreateView, TokenDestroyView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)
from wagtail import hooks

from .api.v1 import views as api_v1_views


@hooks.register("API_V1_VIEWSET_HOOK", order=1)
def register_some_viewsets():
    return {
        "prefix": "users",
        "viewset": api_v1_views.UserViewSet,
        "basename": "user",
    }


@hooks.register("API_V1_VIEW_HOOK")
def register_token_create_view():
    return {
        "regex": True,
        "url_path": r"^auth/token/login/?$",
        "view_class": TokenCreateView,
        "name": "login",
    }


@hooks.register("API_V1_VIEW_HOOK")
def register_token_destroy_view():
    return {
        "regex": True,
        "url_path": r"^auth/token/logout/?$",
        "view_class": TokenDestroyView,
        "name": "logout",
    }


@hooks.register("API_V1_VIEW_HOOK")
def register_jwt_create_view():
    return {
        "regex": True,
        "url_path": r"^auth/jwt/create/?",
        "view_class": TokenObtainPairView,
        "name": "jwt-create",
    }


@hooks.register("API_V1_VIEW_HOOK")
def register_jwt_refresh_view():
    return {
        "regex": True,
        "url_path": r"^auth/jwt/refresh/?",
        "view_class": TokenRefreshView,
        "name": "jwt-refresh",
    }


@hooks.register("API_V1_VIEW_HOOK")
def register_jwt_verify_view():
    return {
        "regex": True,
        "url_path": r"^auth/jwt/verify/?",
        "view_class": TokenVerifyView,
        "name": "jwt-verify",
    }
