from django.template import library

from glamour.partners.models import Team

register = library.Library()


@register.simple_tag
def get_teams(num, *args, **kwargs):
    teams = Team.objects.all()[:num]
    return teams
