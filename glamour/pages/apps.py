from django.apps import AppConfig


class GlamourAppsPagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'glamour.pages'
    label = 'glamour_pages'
