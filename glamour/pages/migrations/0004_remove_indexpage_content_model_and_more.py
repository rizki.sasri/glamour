# Generated by Django 4.0.7 on 2022-08-09 17:48

from django.db import migrations, models
import django.db.models.deletion
import wagtail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0024_index_image_file_hash'),
        ('wagtailcore', '0069_log_entry_jsonfield'),
        ('glamour_pages', '0003_event_news_homepage_content_homepage_cover_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='indexpage',
            name='content_model',
        ),
        migrations.AlterField(
            model_name='indexpage',
            name='template_name',
            field=models.CharField(choices=[('default_page', 'Index Page')], default='default_page', max_length=255, verbose_name='template name'),
        ),
        migrations.CreateModel(
            name='ModelPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('content', wagtail.fields.RichTextField(null=True, verbose_name='content')),
                ('content_model', models.CharField(choices=[('glamour_partners.Partner', 'Partner')], default='wagtail.Page', max_length=255, verbose_name='model name')),
                ('template_name', models.CharField(choices=[('default_page', 'Index Page')], default='default_page', max_length=255, verbose_name='template name')),
                ('cover', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
    ]
