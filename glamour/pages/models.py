from django.apps import apps
from django.db import models  # NOQA
from django.utils.translation import gettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
    RichTextFieldPanel,
)
from wagtail.core.fields import RichTextField
from wagtail.core.models import Orderable, Page
from wagtail.images.edit_handlers import ImageChooserPanel

from glamour.partners.models import Testimonial


class GalleryImage(Orderable):
    page = ParentalKey(
        Page,
        on_delete=models.CASCADE,
        related_name="gallery_images",
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.CASCADE,
        related_name="+",
    )
    caption = models.CharField(blank=True, max_length=250)
    panels = [
        FieldPanel("image"),
        FieldPanel("caption"),
    ]


class HomePage(Page):
    cover = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content = RichTextField(_("content"), null=True, blank=False)
    testimonials = models.IntegerField(
        default=3,
        verbose_name=_("testimonials"),
        help_text=_("Testimonial numbers"),
    )

    subpage_types = [
        "glamour_pages.SinglePage",
        "glamour_pages.IndexPage",
    ]
    parent_page_type = [
        "wagtailcore.Page",
    ]

    content_panels = Page.content_panels + [
        RichTextFieldPanel("content"),
        FieldPanel("testimonials"),
        InlinePanel("gallery_images", heading=_("Gallery Images")),
    ]

    def get_testimonials(self):
        testimonials = Testimonial.objects.all()[: self.testimonials]
        return testimonials

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context.update({"testimonials": self.get_testimonials()})
        return context


class SinglePage(Page):
    TEMPLATE_CHOICES = [
        ("default_page", "Default Page"),
        ("team_page", "Team Page"),
        ("about_page", "About Page"),
        ("service_page", "Our Service Page"),
        ("warranty_page", "Warranty Page"),
    ]
    cover = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content = RichTextField(_("content"), null=True, blank=False)
    template_name = models.CharField(
        max_length=255,
        default="default-choice",
        choices=TEMPLATE_CHOICES,
        verbose_name=_("template name"),
    )
    # Panels
    content_panels = Page.content_panels + [
        ImageChooserPanel("cover"),
        RichTextFieldPanel("content"),
        InlinePanel("gallery_images", label="Gallery images"),
    ]
    promote_panels = Page.promote_panels + []
    settings_panels = [FieldPanel("template_name")] + Page.settings_panels + []

    def get_template(self, request, *args, **kwargs):
        if request.headers.get("x-requested-with") == "XMLHttpRequest":
            return self.ajax_template or self.template
        else:
            return f"glamour_pages/single_pages/{self.template_name}.html"


class IndexPage(Page):
    CONTENT_TYPES = [
        ("wagtail.Page", "Page"),
        ("glamour_pages.Event", "Event"),
    ]
    TEMPLATE_CHOICES = [
        ("default_page", "Index Page"),
    ]
    cover = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content = RichTextField(_("content"), null=True, blank=False)
    content_model = models.CharField(
        max_length=255,
        default="wagtail.Page",
        choices=CONTENT_TYPES,
        verbose_name=_("model name"),
    )
    template_name = models.CharField(
        max_length=255,
        default="default_page",
        choices=TEMPLATE_CHOICES,
        verbose_name=_("template name"),
    )
    # Panels
    content_panels = Page.content_panels + [
        ImageChooserPanel("cover"),
        RichTextFieldPanel("content"),
    ]
    promote_panels = Page.promote_panels + []
    settings_panels = (
        [
            MultiFieldPanel(
                [
                    FieldPanel("template_name"),
                ],
                heading=_("Content"),
            )
        ]
        + Page.settings_panels
        + []
    )

    def get_template(self, request, *args, **kwargs):
        if request.headers.get("x-requested-with") == "XMLHttpRequest":
            return self.ajax_template or self.template
        else:
            return f"glamour_pages/index_pages/{self.template_name}.html"

    def get_context(self, request, *args, **kwargs):
        object_list = self.get_children().type(Event)
        context = super().get_context(request, *args, **kwargs)
        context.update({"object_list": object_list})
        return context


class ModelPage(Page):
    CONTENT_TYPES = [
        ("glamour_partners.Partner", "Partner"),
    ]
    TEMPLATE_CHOICES = [
        ("default_page", "Index Page"),
    ]
    cover = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    content = RichTextField(_("content"), null=True, blank=False)
    content_model = models.CharField(
        max_length=255,
        default=None,
        choices=CONTENT_TYPES,
        verbose_name=_("model name"),
    )
    template_name = models.CharField(
        max_length=255,
        default="default_page",
        choices=TEMPLATE_CHOICES,
        verbose_name=_("template name"),
    )
    # Panels
    content_panels = Page.content_panels + [
        ImageChooserPanel("cover"),
        RichTextFieldPanel("content"),
    ]
    promote_panels = Page.promote_panels + []
    settings_panels = (
        [
            MultiFieldPanel(
                [
                    FieldPanel("content_model"),
                    FieldPanel("template_name"),
                ],
                heading=_("Content"),
            )
        ]
        + Page.settings_panels
        + []
    )

    def get_template(self, request, *args, **kwargs):
        if request.headers.get("x-requested-with") == "XMLHttpRequest":
            return self.ajax_template or self.template
        else:
            return f"glamour_pages/index_pages/{self.template_name}.html"

    def get_context(self, request, *args, **kwargs):
        model = apps.get_model(self.content_model)
        object_list = model.objects.all()
        context = super().get_context(request, *args, **kwargs)
        context.update({"object_list": object_list})
        return context


class TaggedNews(TaggedItemBase):
    content_object = ParentalKey(
        "News",
        related_name="tagged_items",
        on_delete=models.CASCADE,
    )

    class Meta(TaggedItemBase.Meta):
        verbose_name = _("Tags")
        verbose_name_plural = _("Content Tags")


class News(Page):
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    tags = ClusterTaggableManager(through=TaggedNews, blank=True)
    content = RichTextField(_("content"), null=True, blank=False)

    # Panels
    content_panels = Page.content_panels + [
        FieldPanel("tags"),
        ImageChooserPanel("thumbnail"),
        RichTextFieldPanel("content"),
        InlinePanel("gallery_images", label="Gallery images"),
    ]
    promote_panels = Page.promote_panels + []
    settings_panels = Page.settings_panels + []

    subpage_types = []
    parent_page_type = [
        "glamour_pages.IndexPage",
    ]


class TaggedEvent(TaggedItemBase):
    content_object = ParentalKey(
        "Event",
        related_name="tagged_items",
        on_delete=models.CASCADE,
    )

    class Meta(TaggedItemBase.Meta):
        verbose_name = _("Tags")
        verbose_name_plural = _("Content Tags")


class Event(Page):
    thumbnail = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    tags = ClusterTaggableManager(through=TaggedEvent, blank=True)
    content = RichTextField(_("content"), null=True, blank=False)

    # Panels
    content_panels = Page.content_panels + [
        FieldPanel("tags"),
        ImageChooserPanel("thumbnail"),
        RichTextFieldPanel("content"),
        InlinePanel("gallery_images", label="Gallery images"),
    ]
    promote_panels = Page.promote_panels + []
    settings_panels = Page.settings_panels + []

    subpage_types = []
    parent_page_type = [
        "glamour_pages.IndexPage",
    ]
