from django.apps import AppConfig


class GlamourAppsProductsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glamour.products"
    label = "glamour_products"
