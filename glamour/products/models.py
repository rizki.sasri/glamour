from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel

from ..base.mixins import PageMixin


class Product(PageMixin, PolymorphicModel):

    created_at = models.DateTimeField(
        default=timezone.now,
        editable=False,
        verbose_name=_("expired"),
    )
    name = models.CharField(_("name"), max_length=50)

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return self.name
